{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "permanent-amino",
   "metadata": {},
   "source": [
    "# \"Local\" SNR"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "binding-insulin",
   "metadata": {},
   "outputs": [],
   "source": [
    "%run ../cryoem-common/cryoem_common.ipynb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a1ecbd17",
   "metadata": {},
   "outputs": [],
   "source": [
    "add_latex_commands()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "golden-stable",
   "metadata": {},
   "source": [
    "## Signal-to-noise ratio"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "polish-russell",
   "metadata": {},
   "source": [
    "We want to estimate the [signal-to-noise ratio](https://en.wikipedia.org/wiki/Signal-to-noise_ratio) in a reconstructed volume, as a function of the spatial frequency that we are considering."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "humanitarian-symposium",
   "metadata": {},
   "source": [
    "We start with two reconstructed half-volumes $V_1$ and $V_2$, which are independently reconstructed volumes, each one starting with half the total data. Our best estimation of the original volume is $V = \\frac{V_1 + V_2}{2}$ and of the uncertainty in the reconstruction (the \"noise\") is $N = \\frac{V_1 - V_2}{2}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bd42521e",
   "metadata": {},
   "outputs": [],
   "source": [
    "vol1, voxel_n1, voxel_size1 = get_vol_and_voxel('emd_10418_half_map_1.map')\n",
    "vol2, voxel_n2, voxel_size2 = get_vol_and_voxel('emd_10418_half_map_2.map')\n",
    "\n",
    "assert voxel_size1 == voxel_size2 and voxel_n1 == voxel_n2\n",
    "voxel_size = voxel_size1\n",
    "voxel_n = voxel_n1\n",
    "\n",
    "V = (vol1 + vol2) / 2\n",
    "N = (vol1 - vol2) / 2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb51ca14",
   "metadata": {},
   "source": [
    "This is how they, and their Fourier transform, look like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bf85fd0b",
   "metadata": {},
   "outputs": [],
   "source": [
    "FV = fftn(V)\n",
    "FN = fftn(N)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d634f564",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot(V, title='V', lims=(-voxel_size*voxel_n//2, voxel_size*voxel_n//2))\n",
    "fplot(abs(FV), title='$|\\\\mathcal{F}\\\\{V\\\\}|$', slices=(10, 80, 100))\n",
    "\n",
    "plot(N, title='N', lims=(-voxel_size*voxel_n//2, voxel_size*voxel_n//2))\n",
    "fplot(abs(FN), title='$|\\\\mathcal{F}\\\\{N\\\\}|$', slices=(10, 80, 100))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "amino-bonus",
   "metadata": {},
   "source": [
    "Since each half-volume $V_i$ can be seen as the sum of the true signal $S$ plus some noise $N_i$, that is, $V_i = S + N_i$, we have:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "rotary-contract",
   "metadata": {},
   "source": [
    "$$\n",
    "\\begin{eqnarray}\n",
    "V & = & \\frac{V_1 + V_2}{2} = \\frac{(S + N_1) + (S + N_2)}{2} = S + \\frac{N_1 + N_2}{2} =: S + N_+ \\\\\n",
    "N & = & \\frac{V_1 - V_2}{2} = \\frac{(S + N_1) - (S + N_2)}{2} = \\frac{N_1 - N_2}{2} =: N_-\n",
    "\\end{eqnarray}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "informed-california",
   "metadata": {},
   "source": [
    "(Note that we measure $V_1$ and $V_2$, and thus we have $V$ and $N = N_-$, but we don't know $N_+$, although we expect it to behave like $N_-$, and we don't know $S$, of course, which is what we want to really estimate.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "supposed-chicken",
   "metadata": {},
   "source": [
    "Thus:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "prostate-logan",
   "metadata": {},
   "source": [
    "$$\n",
    "\\begin{eqnarray}\n",
    "\\frac{\\norm{V}^2}{\\norm{N}^2} & = & \\frac{\\norm{S}^2 + 2 \\dot{S}{N_+} + \\norm{N_+}^2}{\\norm{N_-}^2} \\\\\n",
    "    & = & \\frac{\\norm{S}^2}{\\norm{N_-}^2} + \\frac{2}{\\norm{N_-}^2} \\dot{S}{N_+} + \\frac{\\norm{N_+}^2}{\\norm{N_-}^2}\n",
    "\\end{eqnarray}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bound-cornell",
   "metadata": {},
   "source": [
    "where:\n",
    "\n",
    "* $\\dot{\\cdot}{\\cdot}$ is the inner product, $\\dot{V_1}{V_2} = \\sum_\\vect{r} V_1(\\vect{r}) V_2(\\vect{r})$, the sum being over all spatial voxels.\n",
    "* $\\norm{\\cdot}$ is the norm, $\\norm{V} = \\sqrt{\\dot{V}{V}}$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d047ce5d",
   "metadata": {},
   "source": [
    "With this decomposition, the expected value of the first term is similar to the **signal-to-noise ratio** $\\SNR = \\norm{S}^2 / \\,\\E{\\norm{N}^2}$ (it is actually $\\norm{S}^2 \\E{\\frac{1}{\\norm{N}^2}}$). The expected value of the second term is 0 (since $\\bra{S}$ is fixed and $\\ket{N_+}$ is going to \"point to all directions\" with equal probability). And in the limit where the variance of $\\norm{N_\\pm}$ is small, the third term is close to 1 (since both $N_+$ and $N_-$ are estimations of the same noise)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "narrative-hormone",
   "metadata": {},
   "source": [
    "That is:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "israeli-resolution",
   "metadata": {},
   "source": [
    "$$\n",
    "\\E{ \\frac{\\norm{V}^2}{\\norm{N}^2} } \\sim \\SNR + 1\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "juvenile-express",
   "metadata": {},
   "source": [
    "This motivates the introduction of the following statistic as an estimate of the SNR:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "earlier-dutch",
   "metadata": {},
   "source": [
    "$$\n",
    "\\hat{\\SNR} := \\frac{\\norm{V}^2}{\\norm{N}^2} - 1\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "through-arabic",
   "metadata": {},
   "source": [
    "Incidentally, we can also write it in terms of $V_1$ and $V_2$ as:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "controlled-breath",
   "metadata": {},
   "source": [
    "$$\n",
    "\\hat{\\SNR} =\n",
    "  \\frac{\\norm{V}^2}{\\norm{N}^2} - 1 =\n",
    "  \\frac{\\norm{\\frac{V_1 + V_2}{2}}^2}{\\norm{\\frac{V_1 - V_2}{2}}^2} - 1 =\n",
    "  \\frac{\\norm{\\frac{V_1 + V_2}{2}}^2 - \\norm{\\frac{V_1 - V_2}{2}}^2}{\\norm{\\frac{V_1 - V_2}{2}}^2} =\n",
    "  \\frac{\\dot{V_1}{V_2}}{\\norm{N}^2}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "entitled-serbia",
   "metadata": {},
   "source": [
    "since expanding the norms we get $\\norm{\\frac{V_1 \\pm V_2}{2}}^2 = \\frac{1}{4} \\left( \\norm{V_1}^2 + \\norm{V_2}^2 \\pm 2 \\dot{V_1}{V_2} \\right)$ and the corresponding terms in the numerator cancel out. In particular, this hints at $\\dot{V_1}{V_2}$ being a good estimation of the \"signal\" $\\norm{S}^2$.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "circular-mixer",
   "metadata": {},
   "source": [
    "### Comparing to the FSC"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "virgin-index",
   "metadata": {},
   "source": [
    "When taking the volumes \"at a given frequency $f$\" (more on that in the following section), we will define our SNR-like statistic as:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "veterinary-cleaning",
   "metadata": {},
   "source": [
    "$$\n",
    "\\hat{\\SNR}(f) := \\frac{\\norm{V^{\\!f}}^2}{\\norm{N^{\\!f}}^2} - 1 = \\frac{\\dot{V^{\\!f}_1}{V^{\\!f}_2}}{\\norm{N^{\\!f}}^2}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "disciplinary-december",
   "metadata": {},
   "source": [
    "For comparison, the [Fourier Shell Correlation](https://gitlab.com/jordibc/fourier-shell-correlation/) is:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "special-measure",
   "metadata": {},
   "source": [
    "$$\n",
    "\\FSC(f) = \\frac{\\dotf{F_1}{F_2}}{\\normf{F_1} \\normf{F_2}} = \\frac{\\dot{V^{\\!f}_1}{V^{\\!f}_2}}{\\norm{V^{\\!f}_1} \\norm{V^{\\!f}_2}}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "liked-guard",
   "metadata": {},
   "source": [
    "Thus, the FSC measures something very similar to our estimation of the SNR, but changing the denominator $\\norm{N^{\\!f}}^2 = \\norm{\\frac{V_1^{\\!f}-V_2^{\\!f}}{2}}^2$ for $\\norm{V^{\\!f}_1} \\norm{V^{\\!f}_2}$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "supposed-annual",
   "metadata": {},
   "source": [
    "## Frequency decomposition"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "continuing-negotiation",
   "metadata": {},
   "source": [
    "Instead of looking at the original volumes, we can work with volumes \"at a frequency $f$\", which are defined as $V^{\\!f} = \\IF{S_f \\times \\F{V}}$, where $\\F{\\cdot}$ is the Fourier transform, and the multiplication by $S_f$ selects only the voxels in frequency space that correspond to the given scalar frequency $f$.\n",
    "\n",
    "This $S_f$, the \"*shell at frequency* $f$\", is a function such that $S_f(\\vect{f'})$ is a smooth approximation to $\\delta(\\|\\vect{f'}\\| - f)$. For example, we can use $S_f(\\vect{f'}) = e^{- \\frac{(\\|\\vect{f'}\\| - f)^2}{2 \\sigma^2}}$ for a certain $\\sigma$, that we can take to cover about 1 pixel. (For more information about this, you can check the [notebook on selecting frequencies](https://gitlab.com/jordibc/selecting-frequencies).)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "worthy-journey",
   "metadata": {},
   "outputs": [],
   "source": [
    "f_voxel_width = 4.6  # width (in voxels) of the shell used to select frequencies  \n",
    "f_width = f_voxel_width / (voxel_n * voxel_size)  # frequency width of the shell\n",
    "\n",
    "fx, fy, fz = fftnfreq(voxel_n, d=voxel_size)\n",
    "f_norm = sqrt(fx**2 + fy**2 + fz**2)\n",
    "\n",
    "def shell(f):\n",
    "    \"Return a shell of spatial frequencies, around frequency f\"\n",
    "    return exp(- (f_norm - f)**2 / (2 * f_width**2))\n",
    "    # The values of f_norm (a volume) and f_width will be chosen according to the volume studied."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39d144a9",
   "metadata": {},
   "source": [
    "(We define a volume in frequency space, `f_norm`, that at each frequency voxel $\\vect{f}$ contains its norm $f = \\norm{\\vect{f}}$, so we can reuse it for every call to `shell(f)`.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7ab32628",
   "metadata": {},
   "source": [
    "Just to have an idea of how the shell, the volume and the noise looks like at different frequencies, let's do a few plots:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "62bcb87e",
   "metadata": {},
   "outputs": [],
   "source": [
    "fplot(shell(f=0.2), title='Frequency shell at frequency 0.2 ($\\\\AA^{-1}$)')\n",
    "\n",
    "def vol_at_freq(f):\n",
    "    S = shell(f)\n",
    "    return real(ifftn(S * FV))\n",
    "\n",
    "for f in [0, 0.1, 0.2, 0.3]:  # A^-1\n",
    "    plot(vol_at_freq(f), title='Volume (V) at frequency %g ($\\\\AA^{-1}$)' % f, add_colorbar=True)\n",
    "    \n",
    "def noise_at_freq(f):\n",
    "    S = shell(f)\n",
    "    return real(ifftn(S * FN))\n",
    "\n",
    "for f in [0, 0.1, 0.2, 0.3]:  # A^-1\n",
    "    plot(abs(noise_at_freq(f)), title='Noise (N) at frequency %g ($\\\\AA^{-1}$)' % f, add_colorbar=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "choice-south",
   "metadata": {},
   "source": [
    "With this definition of $V^{\\!f}$, we estimate the SNR at a given frequency with the following statistic defined at each frequency $f$:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "elder-place",
   "metadata": {},
   "source": [
    "$$\n",
    "\\hat{\\SNR}(f) := \\frac{\\norm{V^{\\!f}}^2}{\\norm{N^{\\!f}}^2} - 1\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "appointed-battle",
   "metadata": {},
   "outputs": [],
   "source": [
    "def SNR(f):\n",
    "    \"Return the estimated SNR of the volume at the frequency f\"\n",
    "    S = shell(f)\n",
    "    SFV = abs(S * FV)\n",
    "    SFN = abs(S * FN)\n",
    "    return max([0, sum(SFV * SFV) / sum(SFN * SFN) - 1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "professional-bicycle",
   "metadata": {},
   "source": [
    "where we use that the inner product is the same in frequency space and real space, $\\dot{\\F{V_1}}{\\F{V_2}} = \\dot{V_1}{V_2}$, and thus the norm too, $\\norm{\\F{V}} = \\norm{V}$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "656926e2",
   "metadata": {},
   "source": [
    "Let's see this SNR at different frequencies:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3dc684f5",
   "metadata": {},
   "outputs": [],
   "source": [
    "fmin, fmax = 0, 1 / (2 * voxel_size)  # maximum frequency is Nyquist\n",
    "freqs = linspace(fmin, fmax, 40)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fbd9686b",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "with Pool() as pool:\n",
    "    snrs = pool.map(SNR, freqs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "665cea5d",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.plot(freqs, abs(snrs), '.-', label='SNR')\n",
    "plt.yscale('log')\n",
    "plt.xlabel('$1/d \\\\, (\\\\AA^{-1})$')\n",
    "plt.hlines(1, fmin, fmax, color='green', linestyle='dashed', label='SNR = 1')\n",
    "plt.hlines(1/3, fmin, fmax, color='red', linestyle='dashed', label='SNR = 1/3')\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "553d12e7",
   "metadata": {},
   "source": [
    "### Thresholds"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22fb65ab",
   "metadata": {},
   "outputs": [],
   "source": [
    "def find_resolution(snr_threshold):\n",
    "    \"Return the resolution that corresponds to the given SNR threshold\"\n",
    "    return 1 / linear_interpolation(freqs, snrs, snr_threshold)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19428611",
   "metadata": {},
   "outputs": [],
   "source": [
    "print('The resolution of this volume at SNR=1   is %.2f A.' % find_resolution(snr_threshold=1))\n",
    "print('The resolution of this volume at SNR=1/3 is %.2f A.' % find_resolution(snr_threshold=1/3))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6bd3f0d0",
   "metadata": {},
   "outputs": [],
   "source": [
    "resolution = find_resolution(snr_threshold=1/3)  # this is what we will take as the actual resolution"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "greenhouse-gilbert",
   "metadata": {},
   "source": [
    "## How local can we go?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "simplified-withdrawal",
   "metadata": {},
   "source": [
    "Instead of doing the sum $\\sum_\\vect{r}$ we could just look at the map of $V(\\vect{r})^2/N(\\vect{r})^2 - 1$, and pretend that we are seeing a \"local SNR\". Or even, look at maps of $V^{\\!f}\\!(\\vect{r})^2/N^{\\!f}\\!(\\vect{r})^2 - 1$ and pretend that we are seeing a \"local SNR at a frequency $f$\".\n",
    "\n",
    "The problem is that at each voxel we are *estimating the different values of the noise using a single point*, and thus the uncertainty on our SNR is huge."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91eb25fe",
   "metadata": {},
   "source": [
    "To overcome it, we can take an average of the volume and the noise on their surroundings. Depending on the constantness of the signal and noise in the region, and the amount of voxels we are using for our statistics, we would have a more or less accurate representation of the volume and the noise around that voxel."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e5172383",
   "metadata": {},
   "source": [
    "So, instead of using the value of the volume and noise at each voxel, we can make a map of local SNR by taking:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d61c4be9",
   "metadata": {},
   "source": [
    "$$\n",
    "\\hat{\\SNR}(f, \\vect{r}) = \\frac{\\left< |V^{\\!f}|^2 \\right>_\\vect{r}}{\\left< |N^{\\!f}|^2 \\right>_\\vect{r}} - 1\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d670fdc0",
   "metadata": {},
   "source": [
    "where $\\left< \\cdot \\right>_\\vect{r}$ is an average of the values around the point $\\vect{r}$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ad5a18e1",
   "metadata": {},
   "source": [
    "So, basically, this is just taking the maps $|V^{\\!f}|^2, |N^{\\!f}|^2$, smoothing them, and divide them and subtract 1 to create another map which is an estimation of the local SNR."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e7159bce",
   "metadata": {},
   "outputs": [],
   "source": [
    "r_voxel = 5  # radius in voxels for the smoothing\n",
    "r = r_voxel * voxel_size  # radius in A\n",
    "\n",
    "# Map to multiply in frequency space to smooth over a radius r\n",
    "smooth = exp(-np.pi**2 * f_norm**2 * r**2)\n",
    "\n",
    "def average(ar):\n",
    "    \"Return a map where every voxel has been smoothed\"\n",
    "    return real(ifftn(fftn(ar) * smooth))\n",
    "\n",
    "\n",
    "def snr_local(f):\n",
    "    \"Return a map of the local estimation of the SNR at frequency f\"\n",
    "    S = shell(f)\n",
    "\n",
    "    Vf = real(ifftn(S * FV))\n",
    "    Vf2_avg = average(Vf**2)\n",
    "\n",
    "    Nf = real(ifftn(S * FN))\n",
    "    Nf2_avg = average(Nf**2)\n",
    "\n",
    "    return np.maximum(0, Vf2_avg / Nf2_avg - 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25a0d121",
   "metadata": {},
   "source": [
    "Let's look at the SNRs \"at a given frequency\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "01e5851c",
   "metadata": {},
   "outputs": [],
   "source": [
    "invalid_voxels = ~mask_in_sphere(voxel_n)\n",
    "# NOTE: We could do instead:  logical_and(~mask_in_sphere(voxel_n), V < density_threshold)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d2253562",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "for f in [0, 0.1, 0.2, 0.3]:  # A^-1\n",
    "    snr_map_at_f = snr_local(f)\n",
    "    snr_map_at_f[invalid_voxels] = 0\n",
    "    plot(snr_map_at_f, title='SNR at frequency %g ($\\\\AA^{-1}$)' % f, add_colorbar=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "optimum-native",
   "metadata": {},
   "source": [
    "As a side note, if we wanted to make a map of \"local FSCs\", we could use the relation $\\FSC = \\sqrt{\\frac{\\SNR}{1 + \\SNR}}$ to create them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "sonic-circular",
   "metadata": {},
   "outputs": [],
   "source": [
    "if False:  # change to \"True\" to see the maps\n",
    "    for f in [0, 0.1, 0.2, 0.3]:  # A^-1\n",
    "        snr = snr_local(f)\n",
    "        fsc = sqrt(snr / (1 + snr))\n",
    "        fsc[invalid_voxels] = 0\n",
    "        plot(fsc, title='FSC at frequency %g ($\\\\AA^{-1}$)' % f, add_colorbar=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "79e14f79",
   "metadata": {},
   "source": [
    "Let's save the map of SNR at the resolution that we found before:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "593dcabe",
   "metadata": {},
   "outputs": [],
   "source": [
    "snr_map_at_resolution = snr_local(1/resolution)\n",
    "snr_map_at_resolution[invalid_voxels] = 0\n",
    "\n",
    "with mrcfile.new('snr_at_max_resolution.mrc', overwrite=True) as mrc:\n",
    "    mrc.set_data(snr_map_at_resolution.astype(np.float32))\n",
    "    mrc.voxel_size = voxel_size"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9be7ed8f",
   "metadata": {},
   "source": [
    "### Local resolution from local SNR"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b747a579",
   "metadata": {},
   "source": [
    "We can use the previous local SNR to estimate a map of local resolution, by seeing, for each voxel $\\vect{r}$, at which frequency $f$ the estimated SNR falls below a certain threshold."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "46a36c5a",
   "metadata": {},
   "source": [
    "That is, taking the threshold as 1/3 (consistent with the use in the FSC), the resolution at a point $\\vect{r}$ will be $f_0$ if:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "919bb959",
   "metadata": {},
   "source": [
    "$$\n",
    "\\hat{\\SNR}(f, \\vect{r}) \\ge \\frac{1}{3} \\qquad \\text{ when } f \\le f_0\n",
    "$$\n",
    "\n",
    "and\n",
    "\n",
    "$$\n",
    "\\hat{\\SNR}(f, \\vect{r}) <  \\frac{1}{3} \\qquad \\text{ when } f > f_0\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d3e2a6e5",
   "metadata": {},
   "outputs": [],
   "source": [
    "def freqs_around(f, n=10):\n",
    "    \"Return a list of frequencies around the given one, geometrically spaced\"\n",
    "    factors_min, factors_max = 0.5, 2\n",
    "    return [f * factors_min * (factors_max / factors_min)**(i/n) for i in range(n+1)]\n",
    "\n",
    "resolution_map = 2 * resolution * np.ones_like(V)\n",
    "\n",
    "snr_threshold = 1/3\n",
    "\n",
    "for f in freqs_around(1/resolution, n=10):\n",
    "    snr_map_at_f = snr_local(f)\n",
    "    snr_map_at_f[invalid_voxels] = 0\n",
    "    resolution_map[snr_map_at_f > snr_threshold] = 1/f\n",
    "\n",
    "snr_map_at_resolution = snr_local(1/resolution)\n",
    "snr_map_at_resolution[invalid_voxels] = 0\n",
    "\n",
    "with mrcfile.new('local_resolution_based_on_snr.mrc', overwrite=True) as mrc:\n",
    "    mrc.set_data(resolution_map.astype(np.float32))\n",
    "    mrc.voxel_size = voxel_size"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b051a63",
   "metadata": {},
   "source": [
    "#### A single number for the resolution?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "53a4dfcf",
   "metadata": {},
   "source": [
    "Note that in the previous estimation of the resolution, we assume that such a frequency $f_0$ exists. In general, $\\hat{\\SNR}$ will not be strictly monotonic with $f$, so when we sweep all frequencies and after the inequality changes sign, there could be some frequencies for which $\\hat{\\SNR}$ crosses the threshold again."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9f7d33f6",
   "metadata": {},
   "source": [
    "But in general there will be a minimum frequency $f_\\text{min}$ such that $\\hat{\\SNR}(f, \\vect{r}) > \\frac{1}{3}$ for $f < f_\\text{min}$, and a maximum frequency $f_\\text{max}$ such that $\\hat{\\SNR}(f, \\vect{r}) < \\frac{1}{3}$ for $f > f_\\text{max}$. We could, instead of reporting the single frequency $f_0$, report the interval $[f_\\text{min}, f_\\text{max}]$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d4578297",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2bbd8cf4",
   "metadata": {},
   "source": [
    "<span title=\"doubtful\" style=\"color:red\">NOTE: I don't think the sections below make sense.</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "colonial-israel",
   "metadata": {},
   "source": [
    "### Local power"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "decent-prince",
   "metadata": {},
   "source": [
    "An alternative consists on finding out the \"local power\" of both the reconstructed volume $V$ and the noise $N$ by using the amplitude of its corresponding *monogenic function*. This function can be obtained using the *spiral filter*, which in the frequency domain corresponds to the three functions `Hx, Hy, Hz = spiral_filter(voxel_n, voxel_size)`, applied to the original volume:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "debc7dee",
   "metadata": {},
   "outputs": [],
   "source": [
    "Hx, Hy, Hz = spiral_filter(voxel_n, voxel_size)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e404d3bc",
   "metadata": {},
   "source": [
    "How do they look like? Let's look at one of them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0df6a617",
   "metadata": {},
   "outputs": [],
   "source": [
    "fplot(imag(Hx), 'Spiral filter for x')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ccd2342b",
   "metadata": {},
   "source": [
    "And the local power would be:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "excellent-arlington",
   "metadata": {},
   "outputs": [],
   "source": [
    "def power(V, FV=None):\n",
    "    \"Return a map with the power of the monogenic function constructed from the volume V\"\n",
    "    FV = FV if FV is not None else fftn(V)  # compute the fourier transform if not given\n",
    "\n",
    "    vx, vy, vz = [real(ifftn(Hi * FV)) for Hi in [Hx, Hy, Hz]]\n",
    "    \n",
    "    return V**2 + vx**2 + vy**2 + vz**2  # power = square of the amplitude"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24719f2d",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot(power(V, FV), title='Local power of $V$', add_colorbar=True)\n",
    "plot(power(N, FN), title='Local power of $N$', add_colorbar=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "thrown-reasoning",
   "metadata": {},
   "source": [
    "We can even find this power at a given frequency:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "subsequent-journey",
   "metadata": {},
   "outputs": [],
   "source": [
    "def power_at_f(V, FV=None, f=0):\n",
    "    \"Return a map with the power of the monogenic function constructed from the volume V at frequency f\"\n",
    "    FV = FV if FV is not None else fftn(V)  # compute the fourier transform if not given\n",
    "\n",
    "    SFV = shell(f) * FV\n",
    "\n",
    "    sv = real(ifftn(SFV))  # volume \"at frequency f\"\n",
    "    svx, svy, svz = [real(ifftn(Hi * SFV)) for Hi in [Hx, Hy, Hz]]\n",
    "    \n",
    "    return sv**2 + svx**2 + svy**2 + svz**2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "nasty-lounge",
   "metadata": {},
   "source": [
    "<span title=\"doubtful\" style=\"color:red\">If any of the above made sense</span>, we could estimate the following local SNRs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "prepared-norwegian",
   "metadata": {},
   "outputs": [],
   "source": [
    "def SNR_local():\n",
    "    return power(V, FV) / power(N, FN) - 1  # TODO: estimate the power of the noise using neighbors too\n",
    "\n",
    "def SNR_local_at_f(f):\n",
    "    return power_at_f(V, FV, f) / power_at_f(N, FN, f) - 1  # TODO: same (noise with neighbors too)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8f7f5009",
   "metadata": {},
   "outputs": [],
   "source": [
    "snr_map = SNR_local()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0df16c0f",
   "metadata": {},
   "source": [
    "We have an SNR map for all the voxels, but some voxels we know for sure won't be part of the molecule, and the \"estimated SNR\" there will be useless, and can dominate when representing these maps, so we set them to 0."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "113decf1",
   "metadata": {},
   "outputs": [],
   "source": [
    "invalid_voxels = ~mask_in_sphere(voxel_n)\n",
    "# NOTE: We could do instead:  logical_and(~mask_in_sphere(voxel_n), V < density_threshold)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b42197ff",
   "metadata": {},
   "outputs": [],
   "source": [
    "snr_map[invalid_voxels] = 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f024e3bd",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot(snr_map, title='Total SNR', add_colorbar=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "63c033dd",
   "metadata": {},
   "outputs": [],
   "source": [
    "with mrcfile.new('snr.mrc', overwrite=True) as mrc:\n",
    "    mrc.set_data(snr_map.astype(np.float32))\n",
    "    mrc.voxel_size = voxel_size"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "judicial-belfast",
   "metadata": {},
   "source": [
    "### At a given frequency"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "statewide-royal",
   "metadata": {},
   "source": [
    "Let's look at the SNRs \"at a given frequency\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "warming-emergency",
   "metadata": {},
   "outputs": [],
   "source": [
    "for f in [0, 0.1, 0.2, 0.3]:  # A^-1\n",
    "    snr_map_at_f = SNR_local_at_f(f)\n",
    "    snr_map_at_f[invalid_voxels] = 0\n",
    "    plot(snr_map_at_f, title='SNR at frequency %g ($\\\\AA^{-1}$)' % f, add_colorbar=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "major-airfare",
   "metadata": {},
   "source": [
    "Let's save the map of SNR at the resolution that we found before:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cheap-beijing",
   "metadata": {},
   "outputs": [],
   "source": [
    "snr_map_at_resolution = SNR_local_at_f(1/resolution)\n",
    "snr_map_at_resolution[invalid_voxels] = 0\n",
    "\n",
    "with mrcfile.new('snr_at_max_resolution.mrc', overwrite=True) as mrc:\n",
    "    mrc.set_data(snr_map_at_resolution.astype(np.float32))\n",
    "    mrc.voxel_size = voxel_size"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "constant-bahrain",
   "metadata": {},
   "source": [
    "### Local resolution from SNR?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "sublime-survey",
   "metadata": {},
   "source": [
    "We are going to try to compute the local resolution at each pixel by seeing at which frequency the local SNR falls below a certain threshold (and keeping the resolution at that point as the last successfully tested 1/frequency)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "threatened-ivory",
   "metadata": {},
   "outputs": [],
   "source": [
    "def freqs_around(f, n=10):\n",
    "    \"Return a list of frequencies around the given one, geometrically spaced\"\n",
    "    factors_min, factors_max = 0.5, 2\n",
    "    return [f * factors_min * (factors_max / factors_min)**(i/n) for i in range(n+1)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "painted-replacement",
   "metadata": {},
   "outputs": [],
   "source": [
    "resolution_map = 2 * resolution * np.ones_like(V)\n",
    "\n",
    "snr_threshold = 1/3\n",
    "\n",
    "for f in freqs_around(1/resolution, n=10):\n",
    "    snr_map_at_f = SNR_local_at_f(f)\n",
    "    snr_map_at_f[invalid_voxels] = 0\n",
    "    resolution_map[snr_map_at_f > snr_threshold] = 1/f"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "latest-calcium",
   "metadata": {},
   "outputs": [],
   "source": [
    "snr_map_at_resolution = SNR_local_at_f(1/resolution)\n",
    "snr_map_at_resolution[invalid_voxels] = 0\n",
    "\n",
    "with mrcfile.new('local_resolution_based_on_snr.mrc', overwrite=True) as mrc:\n",
    "    mrc.set_data(resolution_map.astype(np.float32))\n",
    "    mrc.voxel_size = voxel_size"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
